## Exponea recommender

This is a repository created for Exponea job application.

To start, copy **dataset_catalog.csv** and **dataset_events.csv** to: data/raw/.

In order to run flask server, run: api/app.py

Example requests can be find in: exp-recommender.postman_collection.json

For recommendations to work, following scripts should be run in respective order:
1. scripts/preprocess_data.py
2. scripts/fit_data.py

Recommender evaluation can be done by running following script after above scripts:
3. scripts/evaluate_model.py

