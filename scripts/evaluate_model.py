import os
import pickle

from recommender.recommend import recommend
from recommender import evaluate

if __name__ == '__main__':

    path = '../data/evaluation/customer_histories.pkl'
    if not os.path.exists(path):
        customer_histories = evaluate.generate_customer_histories()
        pickle.dump(customer_histories, open(path, 'wb'))
    else:
        customer_histories = pickle.load(open(path, 'rb'))

    hits = 0
    for _ in customer_histories:
        product_ids = _[0]
        history = _[1]
        recommended_products = recommend('content-based-user-to-item', history)
        # Count as 1 if any of product_ids bought in future is in recommended products
        hits += int(len(set(recommended_products).intersection(set(product_ids))) > 0)

    hit_ratio = hits/len(customer_histories)
    print(f'Hit ratio is: {hit_ratio}')




