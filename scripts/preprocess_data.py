import pandas as pd

from recommender import preprocess

if __name__ == '__main__':

    df = pd.read_csv('../data/raw/dataset_catalog.csv')

    df_cleaned = preprocess.clean(df)
    # Catch all correct html tags
    df_parsed = preprocess.parse_description(df_cleaned)
    # Catch all missed tags
    df_parsed = preprocess.parse_description(df_parsed)
    df_parsed = preprocess.clean_after_parsing(df_parsed)

    # Remove brands from descriptions
    # df_parsed = preprocess.remove_brands_from_descriptions(df_parsed)

    df_parsed.to_pickle('../data/preprocessed/product_data.pkl')

    event_df = pd.read_csv('../data/raw/dataset_events.csv')
    event_df = event_df[event_df.product_id.isin(df_parsed.product_id.unique())]

    event_df.to_pickle('../data/preprocessed/event_data.pkl')
