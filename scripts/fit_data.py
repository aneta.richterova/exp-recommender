import pickle

from recommender import fit

if __name__ == '__main__':

    df = pickle.load(open('../data/preprocessed/product_data.pkl', 'rb'))
    df = df.set_index('product_id')

    pipeline = fit.prepare_pipeline()
    features_df = fit.fit_pipeline(pipeline, df.description)

    features_df.to_pickle('../data/fitted/product_features.pkl')
