import logging
import re
import pandas as pd

from itertools import chain
from bs4 import BeautifulSoup

logger = logging.getLogger(__name__)


def clean_with_imputation(df: pd.DataFrame) -> pd.DataFrame:
    """Imputes rows which have either no description or unuseful description.

    Args:
        df: Pandas DataFrame with `description` column.

    Returns:
        Pandas DataFrame with cleaned `description` column.
    """
    logger.info('Cleaning item data.')
    df = df.copy()

    # Impute category path in rows with missing or unuseful description and add category path to each description
    df.loc[pd.isnull(df.description), 'description'] = ''

    condition = df.description.apply(lambda x: x.startswith('Product code:'))
    df.loc[condition, 'description'] = ''

    condition = df.description.apply(lambda x: 'TABELA' in x)
    df.loc[condition, 'description'] = ''

    df['description'] = df['description'].str.cat(df['category_path'], sep=' ')
    df['description'] = df['description'].str.cat(df['brand'], sep=' ')
    df['description'] = df['description'].str.cat(df['gender'], sep=' ')
    return df


def clean(df: pd.DataFrame) -> pd.DataFrame:
    """Removes rows which have either no description or unuseful description.

    Args:
        df: Pandas DataFrame with `description` column.

    Returns:
        Pandas DataFrame with cleaned `description` column.
    """
    logger.info('Cleaning item data.')
    df = df.copy()

    # Remove lines with null values
    df = df[~pd.isnull(df.description)]

    # Remove unuseful lines
    condition = df.description.apply(lambda x: x.startswith('Product code:'))
    df = df[~condition]
    condition = df.description.apply(lambda x: 'TABELA' in x)
    df = df[~condition]

    return df


def parse_description(df: pd.DataFrame) -> pd.DataFrame:
    """Parse html descriptions to textual representation.

    Args:
        df: Pandas DataFrame with column `description`.

    Returns:
        Pandas DataFrame with parsed column `description`.
    """
    logger.info('Parsing item descriptions.')
    df = df.copy()

    def parse_html(html_text):
        return BeautifulSoup(html_text, 'lxml').text

    df['description_old'] = df['description']
    df['description'] = df.description.apply(parse_html)

    # Sometimes parsing results in empty string, in that case we will leave the original descrpiton there
    condition = df.description == ''
    df.loc[condition, 'description'] = df.loc[condition, 'description_old']
    return df.drop('description_old', axis=1)


def clean_after_parsing(df: pd.DataFrame) -> pd.DataFrame:
    """Clean rows containing unuseful descriptions which were created after html parsing.

    Args:
        df: Pandas DataFrame with column `description`.

    Returns:
        Pandas DataFrame with cleaned column `description`.
    """
    logger.info('Cleaning after parsing.')
    df = df.copy()
    df.description = df.description.apply(lambda x: re.sub('Product code: [0-9]*', '', x))
    return df[df.description != '']



