import pickle

from typing import List, Tuple


def generate_customer_histories() -> List[Tuple]:
    """Generates test data for evaluation of recommender.

    Generates tuples: (customer history, future purchases).

    Returns:
        List of tuples (future purchases, customer history).
    """
    event_df = pickle.load(open('../data/preprocessed/event_data.pkl', 'rb'))
    event_df = event_df.sort_values('timestamp')
    customer_ids = event_df[event_df.type == 'purchase_item'].customer_id.unique().tolist()

    # Filter out only customers which bought something
    event_df = event_df[event_df.customer_id.isin(customer_ids)]

    user_histories = []
    for customer_id, customer_data in event_df.groupby('customer_id'):
        purchased_products = customer_data[customer_data.type == 'purchase_item']

        # Take all history before each purchase
        for _, purchase in purchased_products.iterrows():
            history = customer_data[customer_data.timestamp < purchase.timestamp]
            purchased_product_ids = purchased_products[purchased_products.timestamp >= purchase.timestamp] \
                .product_id.tolist()
            # Remove from history products which were than purchased by a customer
            history = history[~history.product_id.isin(purchased_product_ids)]
            user_histories.append((purchased_product_ids, history))

    return user_histories
