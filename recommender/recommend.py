import logging
import pickle
import pandas as pd

from typing import List
from sklearn.metrics.pairwise import cosine_similarity

logger = logging.getLogger(__name__)


def recommend(model_name: str, customer_history: pd.DataFrame, num_of_products: int = 10) -> List[int]:
    """Recommends products for customer with `customer_history`, based on previous site interactions.

    Based on items viewed/purchased/added_to_cart, calculates customer profile.
    Customer profile is than compared to all available items and
    `num_of_products` most similar features are returned.

    Args:
        model_name: Name of model to use for recommendations. Possible values: [content-based-user-to-item].
        customer_history: History of interactions. Pandas DataFrame with column: `product_id`.
        num_of_products: Number of products which will be recommended.

    Returns:
        List of recommended `product_ids`.
    """

    if model_name == 'content-based-user-to-item':
        logger.info('Generating user profile..')
        products = pickle.load(open('../data/fitted/product_features.pkl', 'rb'))
        customer_history = customer_history.drop_duplicates('product_id', keep='first')

        chosen_products = customer_history[['product_id']].merge(products, on='product_id', how='inner')

        # Sum features thru all historical products to create a customer profile
        customer_profile = chosen_products.drop(['product_id'], axis=1).sum(axis=0)

        logger.info('Searching for best products for customer...')
        # We will search for similarities only with those products, which were not already in the customer history
        historical_product_ids = customer_history.product_id.unique().tolist()
        products_notin_history = products[~products.index.isin(historical_product_ids)]
        similarities = cosine_similarity(products_notin_history, customer_profile.values.reshape(1, -1))
        similarities = pd.Series(similarities[:, 0], index=products_notin_history.index).sort_values(ascending=False)
        return list(similarities.index[:num_of_products])

    raise ValueError(f'Model {model_name} is not implemented.')


