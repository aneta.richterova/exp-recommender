import logging
import pandas as pd

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from sklearn.decomposition import TruncatedSVD

logger = logging.getLogger(__name__)


def prepare_pipeline() -> Pipeline:
    """Prepares transformation pipeline for product features.

    Returns:
        Pipeline object.
    """
    logger.info('Preparing pipeline.')
    vectorizer_params = dict(ngram_range=(1, 2), stop_words='english')
    return Pipeline([
        ('tfidf', TfidfVectorizer(**vectorizer_params)),
        ('svd', TruncatedSVD(100))
    ])


def fit_pipeline(pipeline: Pipeline, descriptions: pd.Series) -> pd.DataFrame:
    """Fits prepared pipeline on product `descriptions`.

    Args:
        pipeline: Prepared pipeline.
        descriptions: Pandas Series of unique product descriptions.

    Returns:
        Pandas DataFrame with fitted feature vectors for each product.
    """
    logger.info('Fitting pipeline.')
    feature_matrix = pipeline.fit_transform(descriptions.values)
    return pd.DataFrame(feature_matrix, index=descriptions.index)
