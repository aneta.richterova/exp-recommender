import json
import numpy as np

from flask import Flask, request

from utils import load_customer_history
from utils import load_products_info
from recommender.recommend import recommend

app = Flask(__name__)


@app.route('/customer-history/', methods=['GET'])
def get_customer_history() -> str:
    """Gets all available customer history based on `customerid`.

    Returns:
        JSON string with customer history.
    """
    customer_id = int(request.args.get('customerid'))
    customer_history = load_customer_history(customer_id, np.inf)
    products = load_products_info(customer_history.product_id.unique().tolist())
    customer_history = customer_history.merge(products, on='product_id', how='left')
    return json.dumps(customer_history.to_dict(orient='records'))


@app.route('/product/', methods=['GET'])
def get_product() -> str:
    """Gets product information based on `productid` parameter.

    Returns:
        JSON string with product information.
    """
    product_id = int(request.args.get('productid'))
    return json.dumps(load_products_info([product_id]).to_dict())


@app.route('/recommend/', methods=['POST'])
def get_recommendation() -> str:
    """Recommends products based on `customer_id` and `timestamp` parameters.

    Parameters are provided in request body.

    Returns:
        JSON string of recommended products in ranked order.
    """
    input_dict = json.loads(request.data)

    customer_id = int(input_dict['customerId'])
    timestamp = int(input_dict['timestamp'])
    num_of_products = int(input_dict['numOfProducts'])

    customer_history = load_customer_history(customer_id, timestamp)
    recommended_product_ids = recommend('content-based-user-to-item', customer_history, num_of_products)
    products = load_products_info(recommended_product_ids)
    products.reset_index(inplace=True)
    products.rename({'index': 'rank'}, axis=1, inplace=True)
    return json.dumps(products.to_dict(orient='records'))


if __name__ == '__main__':
    app.run(host='localhost', port=8080, debug=True)
