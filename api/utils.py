import pickle
import pandas as pd

from typing import List


def load_products_info(product_ids: List[int]) -> pd.DataFrame:
    df = pickle.load(open('../data/preprocessed/product_data.pkl', 'rb'))
    df = df.set_index('product_id')
    return df.loc[product_ids, :].reset_index()


def load_customer_history(customer_id: int, timestamp: int) -> pd.DataFrame:
    event_df = pickle.load(open('../data/preprocessed/event_data.pkl', 'rb'))
    return event_df[(event_df.customer_id == customer_id) & (event_df.timestamp < timestamp)]